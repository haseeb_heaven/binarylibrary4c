#ifndef _BINARY4C_H_
#define _BINARY4C_H_

/*
Binary4c library provides set of method to convert any radix base to binary,and efficient methods
to create and store binary arrays and methods to print binary to (stdout) or to FILE.

Library Uses bitwise operators and standard fixed DataTypes to achieve maximum efficiency and immense speed.

NOTE : This ain't conversion library it wont provide Hex-to-octal or Octal-to-Base32 conversion or stuff like that
it will only provide binary from other radix base .

The AIM of this library is only to provide the most efficient way of getting binary representation in different forms
like creating BinaryArray from large numbers without a hitch, printing binary to output (stdout) or to FILE with very little effort from user,
while still maintaning efficiency through-out the whole application with convenient and intuitive methods to use.

Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 08/11/2017
*/

/*Version V 1.0.0

Summary of methods included.

1)Binary from Decimal (long long unsigned).
2)Binary from Decimal (string format).
3)Binary from Octal.
4)Binary from Hexa-decimal.
5)Binary from Base32.

Version V 1.0.1  - Dated 24/11/2017

Summary of methods added.
6)Binary from Floating-point number.
7)Binary from Double Floating-point number.


Summary of Radix bases supported for binary representation.
1)Octal.		 [base-8]
2)Decimal.		[base-10]
3)Float/Double. [base-10]
4)Hexa-dec		[base-16]
5)Base32		[base-32]
*/


#include<stdio.h> /*For I/O Operations */
#include<stdlib.h> /*For For malloc() ,exit() */
#include<string.h>	/*for memset() */
#include<stdint.h> /*For standard data types */
#include<math.h>  /*For log2l() & floor() */
#include<time.h> /*For clock generation */
#include<stdbool.h> /*For boolean*/

#define BYTE  8	  /*Defining single byte*/
#define WORD  16 /*Defining single word*/
#define DWORD 32 /*Defining double word*/
#define QWORD 64 /*Defining quad word*/

#define BINARY_BASE 2 /*Defining binary base*/
#define OCTAL_BASE 8  /*Defining octal base*/
#define DECIMAL_BASE 10 /*Defining decimal base*/
#define HEXA_BASE 16	/*Defining hexa-decimal base*/
#define BASE32_BASE 32 /*Defining base32 base*/

#define FLOAT_INDEX 1 /*Defining Float-index for Real-index*/
#define DOUBLE_INDEX 2  /*Defining Double-index for Real-index*/

/*Defining Variadic MACRO to take optional argument.
NOTE : this won't work below C-99 standard so in that case just use printBinaryArray() method directly */

#define binArrayVariadic(x,y,...) (x),(y)
#define printBinary4mArray(...) printBinaryArray(binArrayVariadic(__VA_ARGS__,getBinaryArrayLen(),0))


/*Global variable to store length Binary Array*/
static uint8_t binaryArrayLen = 0;

/****************************************************************************/
/*********************-PUBLIC-METHODS-***************************************/
/****************************************************************************/

/*Public methods to provide binary representation*/
uint8_t* getBinary4mDecimal(const char* decStr);
uint8_t* getBinary4mOctal(const char* octalStr);
uint8_t* getBinary4mHex(const char* hexStr);
uint8_t* getBinary4mBase32(const char* base32Str);
uint8_t* getBinary4mFloat(float floatNum);
uint8_t* getBinary4mDouble(double doubleNum);
uint8_t* getBinaryArray(uint64_t num);

/*Public methods to print or write binary representation to stdout or to FILE*/
void printBinary4mDecimal(uint64_t dec);
void printBinary4mDecimalStr(const char *decStr);
void printBinary4mOctal(const char *octalStr);
void printBinary4mHex(const char* hexStr);
void printBinary4mBase32(const char* base32Str);
void printBinary4mFloat(float floatNum);
void printBinary4mDouble(double doubleNum);
void printBinary4mDecimalInBits(uint64_t num,int8_t bits);
bool printBinary4mDecimal2File(uint64_t num,FILE *fp);
void printBinaryArray(uint8_t *ptr2BinArray,uint8_t binArrayLen);
bool writeBinary4mRange(uint64_t startRange,uint64_t endRange,FILE *ptr2Output);

/*Public method to get binary array length*/
uint8_t  getBinaryArrayLen();

/****************************************************************************/
/****************-SEMI-PRIVATE-METHODS-**************************************/
/****************************************************************************/
/*Semi-private helper methods*/
uint64_t getDecimal4mBinary(const char *binaryStr);
uint64_t getDecimal4mOctal(const char *octalStr);
uint64_t getDecimal4mDecimal(const char *decStr);
uint64_t getDecimal4mHex(const char *hexStr);
uint64_t getDecimal4mBase32(const char *base32Str);
uint64_t getDecimal4mFloat(float floatNum);
uint64_t getDecimal4mDouble(double doubleNum);
uint64_t getDecimal4mReal(double realNum,int8_t realIndex);

/*Semi-private methods for decimal number*/
uint64_t getDecimal4mArray(uint8_t *ptr2BinArray,uint8_t binArrayLen);

/*Semi-private utility methods for bits*/
const int8_t getEncodingBits(uint64_t num);
const int8_t getRoundedEncodedBits(int8_t bits);
void setBitsAt(uint8_t *dest,uint64_t source,uint8_t pos);
bool isBitSetAtPos(uint8_t num,uint8_t pos);

/****************************************************************************/
/****************-PRIVATE-METHODS-*******************************************/
/****************************************************************************/

/*Private utility methods for binaryArray*/
long double negLog2(uint64_t num);
uint64_t getValue4Byte(uint8_t *ptr2Byte,int8_t byteIndex);
uint64_t getBitValue4mPosition(int8_t pos, int8_t byteIndex);

/*Private utility methods for bits*/
const int8_t getBits4mBytes(int8_t bytes);
const int8_t getBytes4mBits(int8_t bits);

/*Private booleans */
bool isNumInMaxRange(uint64_t num);
bool isNextByte(int8_t	bitIndex);

/**
 * INFO : This is the main method for storing binary from any radix base in whole library.
 * @description -  Get binary from Decimal number.
 * @param - Decimal number , max unsigned long long.
 * @return - binary equivalent of Number in form of pointer to BinaryArray[]
 * NOTE : free this memory after using it to avoid memory leaks.
 */
uint8_t* getBinaryArray(uint64_t num)
{
    int8_t encodeBitIndex = 0,byteIndex = -1;
    int8_t encodeBits = getRoundedEncodedBits(getEncodingBits(num));

    binaryArrayLen = getBytes4mBits(encodeBits);
    binaryArrayLen = (binaryArrayLen <= 0 || binaryArrayLen > BYTE) ? 0x1 : binaryArrayLen; /*Sanity check for size > 64bits*/

    int8_t bitIndex = 0,binValue;
    uint8_t *binaryArray = malloc(binaryArrayLen + 1); /*Dynamic Array to store binary equivalent*/

    if(binaryArray == NULL)
    {
        perror("Error while allocating memory ");
        exit(EXIT_FAILURE);
    }

    memset(binaryArray,0x0,binaryArrayLen); /*Set 0 as initial value to Array*/

    /*Storing binary equivalent in 1-bit each of binaryArray*/
    for (encodeBitIndex = 0; encodeBitIndex < encodeBits; encodeBitIndex++,bitIndex++)
    {

        if(isNextByte(encodeBitIndex))
        {
            byteIndex += 1;
            bitIndex = 0; /*-_- reset bitIndex for every byte*/
        }

        binValue = ((num >> encodeBitIndex) & 1) ?  1 : 0;
        setBitsAt((binaryArray + byteIndex),binValue,bitIndex);
    }
    binaryArray[binaryArrayLen] = '\0'; /*insert NULL at end */
    return binaryArray;
}

/**
 * @description - Get decimal from Base32 string.
 * @param - Base32-Number in String format case insensitive.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mBase32(const char *base32Str)
{
    char *endBase32Str;
    errno = 0;

    uint64_t dec4mBase32 = strtoull(base32Str,&endBase32Str,BASE32_BASE);

    if (*endBase32Str != '\0' || errno != 0)
    {
        fprintf(stderr,"Invalid base32-number encountered : %c\n",*endBase32Str);
        exit(EXIT_FAILURE);
    }
    return dec4mBase32;
}


/**
 * @description - Get decimal from hexa-decimal string.
 * @param - hexa-decimal in String format,case insensitive.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mHex(const char *hexStr)
{
    char *endHexStr;
    errno = 0;

    uint64_t dec4mHex = strtoull(hexStr,&endHexStr,HEXA_BASE);

    if (*endHexStr != '\0' || errno != 0)
    {
        fprintf(stderr,"Invalid hexa-decimal encountered : %c\n",*endHexStr);
        exit(EXIT_FAILURE);
    }
    return dec4mHex;
}

/**
 * @description - Get decimal from Octal string.
 * @param - Octal-Number in String format.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mOctal(const char *octalStr)
{
    char *endOctalStr;
    errno = 0;

    uint64_t dec4mOctal = strtoull(octalStr,&endOctalStr,OCTAL_BASE);

    if (*endOctalStr != '\0' || errno != 0)
    {
        fprintf(stderr,"Invalid octal-number encountered : %c\n",*endOctalStr);
        exit(EXIT_FAILURE);
    }
    return dec4mOctal;
}

/**
 * @description - Get decimal from Decimal string.
 * @param - Decimal-Number in String format.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mDecimal(const char *decStr)
{
    char *endDecStr;
    errno = 0;

    uint64_t dec4mDecStr = strtoull(decStr,&endDecStr,DECIMAL_BASE);

    if (*endDecStr != '\0' || errno != 0)
    {
        fprintf(stderr,"Invalid decimal-number encountered : %c\n",*endDecStr);
        exit(EXIT_FAILURE);
    }
    return dec4mDecStr;
}

/**
 * @description - Get decimal from Binary string.
 * @param - Binary-Number in String format.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mBinary(const char *binaryStr)
{
    char *endBinaryStr;
    errno = 0;

    uint64_t dec4mBinary = strtoull(binaryStr,&endBinaryStr,BINARY_BASE);

    if (*endBinaryStr != '\0' || errno != 0)
    {
        fprintf(stderr,"Invalid binary-number encountered : %c\n",*endBinaryStr);
        exit(EXIT_FAILURE);
    }
    return dec4mBinary;
}


/**
 * @description - Get decimal from Floating-point number
 * @param - Floating-point number in float format.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mFloat(float floatNum)
{
    return	getDecimal4mReal(floatNum,FLOAT_INDEX);
}

/**
 * @description - Get decimal from Double Floating-point number
 * @param - Floating-point number in Double format.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mDouble(double doubleNum)
{
    return	getDecimal4mReal(doubleNum,DOUBLE_INDEX);
}

/**
 * @description - Get decimal from Real Floating-point number
 * @param - Floating-point number in Float or Double format, and realNumber Index.
 * @return - decimal equivalent in uint64_t format
 */
uint64_t getDecimal4mReal(double realNum,int8_t realIndex)
{

    uint64_t dec4mReal = 0;
    uint8_t *ptr2Array = NULL;
    uint8_t size = (realIndex == FLOAT_INDEX) ? sizeof(float) : sizeof(double);
    uint8_t	realArray[size], index;
    float floatNum = realNum;
    double doubleNum = realNum;
    void *realNumPtr = NULL;

    realNumPtr = (realIndex == FLOAT_INDEX) ? (float*)&floatNum : (double*)&doubleNum;

    for(index = 0; index < size; index++)
    {
        realArray[index] = *((uint8_t*)(realNumPtr) + index);
        ptr2Array = getBinaryArray(realArray[index]);
        dec4mReal += getValue4Byte(ptr2Array,index + 1);
        free(ptr2Array);
    }
    return dec4mReal;
}

/**
 * @description - Get binary from Base32-number String.
 * @param - Base32-number in String format,case insensitive.
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leaks.
 */
uint8_t* getBinary4mBase32(const char* base32Str)
{
    return getBinaryArray(getDecimal4mBase32(base32Str));
}


/**
 * @description - Get binary from hexa-decimal string.
 * @param - hexa-decimal in String format,case insensitive.
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leaks.
 */
uint8_t* getBinary4mHex(const char* hexStr)
{
    return getBinaryArray(getDecimal4mHex(hexStr));
}

/**
 * @description - Get binary from octal string.
 * @param - Octal-number in String format
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leaks.
 */
uint8_t* getBinary4mOctal(const char* octalStr)
{
    return getBinaryArray(getDecimal4mOctal(octalStr));
}

/**
 * @description - Get binary from decimal string.
 * @param - decimal-number in String format
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leaks.
 */
uint8_t* getBinary4mDecimal(const char* decStr)
{
    return getBinaryArray(getDecimal4mDecimal(decStr));
}

/**
 * @description - Get binary from Floating-point number.
 * @param - Floating-point number in float format.
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leaks.
 */
uint8_t* getBinary4mFloat(float floatNum)
{
    return getBinaryArray(getDecimal4mFloat(floatNum));
}


/**
 * @description - Get binary from Double-Floating-point number.
 * @param - Floating-point number in Double format.
 * @returns - pointer to binary in uint8_t format or NULL on error
 * NOTE : free this memory after using it to avoid memory leaks.
 */
uint8_t* getBinary4mDouble(double doubleNum)
{
    return getBinaryArray(getDecimal4mDouble(doubleNum));
}


/**
 * @description - Prints binary equivalent from Base32 String.
 * @param - Base32-number in String format,case insensitive.
 */
void printBinary4mBase32(const char *base32Str)
{
    uint64_t decimal4mBase32 = getDecimal4mBase32(base32Str);
    printBinary4mDecimal(decimal4mBase32);
}


/**
 * @description - Prints binary equivalent from Hexa decimal String.
 * @param - hexa-decimal in String format,case insensitive.
 */
void printBinary4mHex(const char *hexStr)
{
    uint64_t decimal4mHex = getDecimal4mHex(hexStr);
    printBinary4mDecimal(decimal4mHex);
}

/**
 * @description - Prints binary equivalent from Octal String.
 * @param - octal-number in String format.
 */
void printBinary4mOctal(const char *octalStr)
{
    uint64_t decimal4mOctal = getDecimal4mOctal(octalStr);
    printBinary4mDecimal(decimal4mOctal);
}

/**
 * @description - Prints binary equivalent from Decimal String.
 * @param - decimal-number in String format.
 */
void printBinary4mDecimalStr(const char *decStr)
{
    uint64_t decimal = getDecimal4mDecimal(decStr);
    printBinary4mDecimal(decimal);
}

/**
 * @description - Prints binary equivalent from Floating-point number.
 * @param - Floating-point number in Float format.
 */
void printBinary4mFloat(float floatNum)
{
    uint64_t decimal4mFloat = getDecimal4mFloat(floatNum);
    printBinary4mDecimal(decimal4mFloat);
}

/**
 * @description - Prints binary equivalent from Double Floating-point number.
 * @param - Floating-point number in Double format.
 */
void printBinary4mDouble(double doubleNum)
{
    uint64_t decimal4mDouble = getDecimal4mDouble(doubleNum);
    printBinary4mDecimal(decimal4mDouble);
}


/**
 * @description - Prints binary equivalent from decimal number.
 * @param - Decimal number in uint64_t format.
 */
void printBinary4mDecimal(uint64_t dec)
{
    int8_t bits = getEncodingBits(dec);
    printBinary4mDecimalInBits(dec,bits);
}

/**
 * INFO : This is the main method for printing binary from any radix base in whole library.
 * @description - Prints binary equivalent from decimal number in N-bits representation.
 * @param - Decimal number , No. of bits to represent
 */
void printBinary4mDecimalInBits(uint64_t num,int8_t bits)
{
    int8_t byteIndex = 0;

    if(bits > QWORD)
    {
        fputs("Bits must not exceed than 64bits!",stderr);
        exit(EXIT_FAILURE);
    }

    printf("\nNumber %llu encoded in %d bits\n\n",num,bits);
    printf("Binary : ");
    for (--bits; bits >= 0; bits--)
    {
        byteIndex = bits + 1;

        if(isNextByte(byteIndex))
            printf("\t");
        printf("%d",( (num >> bits) & 1));
    }
    printf("\n");
}

/**
 * @description - Prints binary equivalent to FILE
 * @param - Decimal number and Pointer to FILE.
 * @returns - true on success or false on failure.
 */
bool printBinary4mDecimal2File(uint64_t num,FILE *fp)
{
    int8_t byteIndex;
    int8_t encodeBits = getEncodingBits(num);

    if(fp != NULL)
    {
        fprintf(fp,"\nBinary of %llu is :\n",num);
        for (--encodeBits; encodeBits >= 0; encodeBits--)
        {
            byteIndex = encodeBits + 1;

            if(isNextByte(byteIndex) && byteIndex != encodeBits)
                fprintf(fp,"\t");
            fprintf(fp,"%d",( (num >> encodeBits) & 1));
        }
        fprintf(fp,"\n");
        return true;
    }
    perror("Error while writing to file : ");
    return false;
}

/**
 * NOTE : This method can write to output to console and to FILE both according to option provided to (ptr2Output).
 * @description - Writes binary to output (stdout or to FILE) accordingly from given range.
 * @param - starting number, ending number, and pointer to Output (ex FILE* or STDOUT)
 * @returns - true if writin success to output else return false.
 */

bool writeBinary4mRange(uint64_t startRange,uint64_t endRange,FILE *ptr2Output)
{

    bool validRange = (startRange < endRange) ? true : false;

    if(ptr2Output != NULL)
    {
        if(validRange)
        {

            /*Creating timer to count time elapsed while generating binary numbers */
            clock_t start;
            double timeUsed;
            start = clock();

            uint64_t num = startRange;
            int8_t bits = getEncodingBits(num);
            int8_t bytes = getBytes4mBits(getRoundedEncodedBits(bits));
            int8_t arrLen = bits + bytes;
            uint8_t binaryArray[arrLen],arrIndex;
            int8_t bitIndex;

            do
            {
                bits = getEncodingBits(num);
                bytes = getBytes4mBits(getRoundedEncodedBits(bits));
                arrLen = bits + bytes;

                arrIndex = arrLen - 1;
                bytes = 0;

                for (bitIndex = 0; bitIndex < bits; bitIndex++,bytes++)
                {

                    if(isNextByte(bytes))
                    {
                        binaryArray[arrIndex--] = '\t';
                    }
                    /*store 1 or 0 in ASCII because fwrite writes in binary */
                    binaryArray[arrIndex--] = ((num >> bitIndex) & 1) ? '1' : '0';

                }

                fprintf(ptr2Output,"\nBinary of %llu is : \n",num);

                /*writing the whole binary array at once thus increasing efficiency and speed*/
                if(fwrite(binaryArray,sizeof(uint8_t),arrLen,ptr2Output) != arrLen)
                    return false;

                fprintf(ptr2Output,"\n");
            }
            while(num++ != endRange);

            timeUsed = ((double) (clock() - start)) / CLOCKS_PER_SEC;
            printf("%llu binary number written in %f seconds\n",endRange - startRange,timeUsed);
        }


        else
        {
            fprintf(stderr,"Invalid range encountered\n");
            exit(EXIT_FAILURE);
        }

    }

    else
    {
        perror("Error occured while writing output : ");
        exit(EXIT_FAILURE);
    }

    return true;
}


/**
 * INFO : This is the main method for printing binaryArray from any radix base in whole library.
 * @description - Prints binary from array which is storing binary equivalent.
 * @param - Pointer to array which is storing binary, and Length of binary array (Optional).
 */
void printBinaryArray(uint8_t *ptr2BinArray,uint8_t binArrayLen)
{
    if(ptr2BinArray == NULL)
    {
        fputs("Binary array must not be empty",stderr);
        exit(EXIT_FAILURE);
    }

    uint64_t num = 0;
    uint8_t nBits = 0;
    uint8_t binaryArrayLength = binArrayLen;

    num = getDecimal4mArray(ptr2BinArray,binaryArrayLength);
    nBits = getEncodingBits(num);

    printBinary4mDecimalInBits(num,nBits);
}

/**
 * @description - Get decimal equivalent from binary array.
 * @param - Pointer to array which is storing binary,and Length of binary array.
  * @returns - decimal equivalent of binary array.
 */
uint64_t getDecimal4mArray(uint8_t *ptr2BinArray,uint8_t binArrayLen)
{
    uint64_t binValue = 0;
    uint8_t binaryArrayLen = binArrayLen;
    if(binaryArrayLen <= 0 || binaryArrayLen > BYTE)
    {
        fputs("Length of binary array must be in range [1 - 8]",stderr);
        exit(EXIT_FAILURE);
    }

    uint8_t index;
    for(index = 0; index < binaryArrayLen; index++)
        binValue += getValue4Byte((ptr2BinArray + index),index + 1);
    return binValue;
}


/**
 * @description - Get the decimal value from specific byte from Binary Array.
 * @param - pointer to binaryArray's byte and byte index
 * @returns - decimal equivalent of binaryArray's byte
 */

uint64_t getValue4Byte(uint8_t *ptr2Byte,int8_t byteIndex)
{
    uint64_t value = 0;
    int8_t bitIndex = 0;

    for(bitIndex = 0; bitIndex < BYTE; bitIndex++)
    {
        if (isBitSetAtPos(*ptr2Byte,bitIndex))
            value += getBitValue4mPosition(bitIndex,byteIndex);
    }
    return value;
}

/**
 * @description - Get Bit value from Byte of BinaryArray
 * @param - position of bit, byte index
 * @returns - value of specific bit in uint64_t format.
 */
uint64_t getBitValue4mPosition(int8_t pos, int8_t byteIndex)
{
    int8_t index;
    int8_t bIndex = 0;

    for(index = 1; index <= BYTE ; index++)
    {
        if(index == byteIndex)
            return (uint64_t)pow(2, (bIndex + pos));
        bIndex += BYTE;
    }
    return 0; /*if index is < 0 or index > BYTE */
}


/**
 * @description - This provides negative Log to the base 2 i.e reciprocal of log2,
 *it uses log2l long double version from math library to implement negative log.
 *Use correct format for long double to avoid precision errors as refrenced.

 * @param - Decimal number (max uint64_t)
 * @returns - negative log to the base 2 in long double format.
 */

long double	negLog2(uint64_t num)
{
    long double negLogVal = 0.0f;
    negLogVal = (num < 0) ? (sizeof(num) * BYTE) : (log2l(1.0L) - log2l(num));
    return isNumInMaxRange(num) ?  fabs(negLogVal) + 1 : negLogVal;
}

/**
 * @description - Encoding Bits in which provided number could be endcoded
 * @param - decimal number
 * @returns - encoded bits or returns QWORD if bits > 64-bit.
 */
const int8_t getEncodingBits(uint64_t num)
{
    int8_t encodingBits = (int8_t)fabs(floor(negLog2(num) - 1) + 1);
    return  (encodingBits > QWORD) ? QWORD : encodingBits;
}

/**
 * @description - Round encoded bits according to BYTE,WORD etc (MAX 64bit representation)
 * @param - number of bits
 * @returns - rounded bits in Range of (bits > BYTE and bits < QWORD) .
 * or rounded QWORD if bits > 64Bit representation.
 */
const int8_t getRoundedEncodedBits(int8_t bits)
{
    int8_t roundedBits;
    for(roundedBits = BYTE; roundedBits <= QWORD; roundedBits+=BYTE)
    {
        if(bits >= 0 && bits <= roundedBits)
            return roundedBits;
    }
    return QWORD;
}

/**
 * @description - Set Bits at specific position
 * @param - pointer to destination , source and position of bit.
 */
void setBitsAt(uint8_t *dest,uint64_t source,uint8_t pos)
{
    uint64_t mask = ((~0ULL) >> (sizeof(uint64_t) * BYTE - QWORD)) << pos;
    *dest = (*dest & ~mask)|((source<<pos) & mask);
}

/**
 * Check Bit present at  specific position
 * @param - number and position of bit .
 */
bool isBitSetAtPos(uint8_t num,uint8_t pos)
{
    return  (1 << pos) & num;
}

/**
 * @description - Get the length of Binary array.
 * @param - none
 * @returns - length of binary array in uint8_t format.
 */

uint8_t getBinaryArrayLen()
{
    return binaryArrayLen;
}


/**
 * @description - Get bits from Bytes.
 * @param - bytes to be converted.
 * @returns - bits in int8_t format.
 */
const int8_t getBits4mBytes(int8_t bytes)
{
    return (bytes * BYTE);
}

/**
 * @description - Get Bytes from Bits.
 * @param - bits to be converted.
 * @returns - bytes in int8_t format.
 */
const int8_t getBytes4mBits(int8_t bits)
{
    return (bits / BYTE);
}

/**
 * @description - Checking number for its range.
 * @param - num to check range.
 * @returns - true if num is in range else returns false.
 */
bool isNumInMaxRange(uint64_t num)
{
    return ((num == (INT8_MAX  + 1U) || (UINT8_MAX  + 1U) ||
             num == (INT16_MAX  + 1U) || (UINT16_MAX  + 1U) ||
             num == (INT32_MAX  + 1ULL) ||(UINT32_MAX  + 1ULL) ))
           ?
           true : false;
}

/**
 * @description - Check if you have reached next Byte
 * @param - index of bit to be checked.
 * @returns - true on nextByte else returns false
 */
bool isNextByte(int8_t	bitIndex)
{
    return ((bitIndex % BYTE) == 0) ? true : false;
}

#endif	/* _BINARY4C_H_ */
