# BinaryLibrary4C-Binay library for creating and printing Binary-Arrays.

Binary4c library provides set of method to convert any radix base to binary and efficient methods
to create and store binary arrays and methods to print binary to (stdout) or to FILE.

Library Uses bitwise operators and standard fixed DataTypes to achieve maximum efficiency and immense speed.

# AIM : This ain't conversion library

it wont provide Hex-to-octal or Octal-to-Base32 conversion or stuff like that
it will only provide binary from other radix base to store and use for later use .

The AIM of this library is only to provide the most efficient way of getting binary representation in different forms
like creating BinaryArray from large numbers without a hitch, printing binary to output (stdout) or to FILE with very little effort from user, 
while still maintaning efficiency through-out the whole application with convenient and intuitive methods to use.

Please use all the docs provided with methods to achieve efficiency.

# How efficiency and speed is achieved in this library.
This lirary uses standatd fixed data types and bitwise operators to achieve maximum efficiency.

# Efficiency in creating Binary-Arrays .
Library allocates minimum 1-Byte to maximum 8-Bytes for storing binary equivalent depends upon length of number 
after than it uses 1-bit of allocated byte to store 1 or 0 in binary using setBitsAt() method thus saving memory.

# Efficiency in printing Binary-Arrays .
Library prints whole array at once in form of buffered chunk using fwrite method to write in binary form making it very efficienct
when printing binary in big ranges.

# NOTE : Free memory avoid leaks.
All methods with getBinary4m....() returns pointer to array so you need to free that memory by yourself.

# Test File for demo.
TestFile included named BinaryTESTS.c run that file to see demo of this library.

Written by HaseeB Mir (haseebmir.hm@gmail.com)
Dated : 11/08/2017
