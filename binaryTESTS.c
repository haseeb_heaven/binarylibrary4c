#include "binary4c.h"


void decimalTest_num(uint64_t);
void decimalTest_str(char *);
void floatTest(float);
void doubleTest(double);
void octalTest(char *);
void hexa_decTest(char *);
void base32Test(char *);
void writeBinary_output(uint64_t,uint64_t);
void writeBinary_file(uint64_t,uint64_t);
char *readInputString();

int main()
{
	 int16_t choice = 0;
	 uint64_t num = 0;
	 uint64_t min,max;
	 float floatNum = 0.0f;
	 double dblNum = 0.0;	
 		
	 do{
	 	printf("\nWelcome to Binary test :)\n");
 		printf("1)Run Decimal Test (string)\n");
 		printf("2)Run Decimal Test (number)\n");
 		printf("3)Run Floating-point Test\n");
 		printf("4)Run Double Floating-point Test\n");
 		printf("5)Run Octal Test\n");
 		printf("6)Run Hexa-decimal Test\n");
 		printf("7)Run Base32 Test\n");
 		printf("8)Run WriteBinaryRange Test Output (stdout)\n");
 		printf("9)Run WriteBinaryRange Test to (FILE)\n");
 		printf("10)Exit\n");
 		printf("Enter your choice number\n");
 		scanf("%d",&choice);
 		
 		switch(choice){
		 	case 1 :
		 	printf("Enter decimal string\n");
 		    char *decStr = readInputString();
		 	decimalTest_str(decStr);
		 	break;
		 	
		 	case 2 :
	 		printf("Enter decimal number\n");
	 		scanf("%llu",&num);
		 	decimalTest_num(num);
		 	break;
		 	
		 	case 3 :
	 		printf("Enter floating-point number\n");
	 		scanf("%f",&floatNum);
		 	floatTest(floatNum);
		 	break;
		 	
		 	case 4 :
	 		printf("Enter Double floating-point number\n");
	 		scanf("%lf",&dblNum);
		 	doubleTest(dblNum);
		 	break;
		 	
		 	case 5 :
	 		printf("Enter octal number\n");
	 		char *octStr = readInputString();
		 	octalTest(octStr);
		 	break;
		 	
		 	case 6 :
	 		printf("Enter hexa-decimal number\n");
	 		char *hexStr = readInputString();
		 	hexa_decTest(hexStr);
		 	break;
		 	
		 	case 7 :
	 		printf("Enter base32 number\n");
	 		char *base32Str = readInputString();
		 	base32Test(base32Str);
		 	break;
		 	
		 	case 8 :
	 		printf("Enter min range\n");
	 		scanf("%llu",&min);
	 		
	 		printf("Enter max range\n");
	 		scanf("%llu",&max);
	 		
		 	writeBinary_output(min,max);
		 	break;
		 	
		 	case 9 :
	 		printf("Enter min range\n");
	 		scanf("%llu",&min);
	 		
	 		printf("Enter max range\n");
	 		scanf("%llu",&max);
		 	writeBinary_file(min,max);
		 	break;
		 	
		 	case 10 :
			 printf("all test-executed successfully exiting...."); 
		 	exit(EXIT_SUCCESS);
		 	
		 	default :
		 	printf("Error wrong choice\n");
		 	exit(1);
		 }
 		
 	}while(choice >=1 && choice <= 10);
 		 
	printf("\n");
	return 0;
}

void decimalTest_num(uint64_t num){
	
	char *ptr = getBinaryArray(num);
	printBinary4mArray(ptr);
	
	printBinary4mDecimal(num);	
	free(ptr);		
}

void decimalTest_str(char *decStr){
	
	char *decStr1 = "255";/*valid decimal*/
	char *decStr2 = "H99";/*invalid decimal*/
	
	char *ptr = getBinary4mDecimal(decStr);
	printBinary4mArray(ptr);
	
	printBinary4mDecimalStr(decStr);	
	free(ptr);		
}

void floatTest(float floatNum){
	
	uint8_t *ptr2FL = getBinary4mFloat(floatNum);
	printBinary4mArray(ptr2FL);
	free(ptr2FL);	
}

void doubleTest(double doubleNum){
	uint64_t __dec4mDouble = getDecimal4mDouble(doubleNum);
	printf("%#llX = ",__dec4mDouble);
	
	uint8_t *ptr2DBL = getBinary4mDouble(doubleNum);
	printBinary4mArray(ptr2DBL);
	free(ptr2DBL);
}

void octalTest(char* octStr){
	
	char *octStr1 = "700"; /*valid octal*/
	char *octStr2 = "999"; /*invalid octal*/
	
	char *ptr = getBinary4mOctal(octStr);
	printBinary4mArray(ptr);
	
	printBinary4mOctal(octStr);	
	free(ptr);		
}

void hexa_decTest(char *hexStr){
	
	char *hexStr1 = "0xFFFF"; /*valid hexa-decimal*/
	char *hexStr2 = "0xLA2"; /*invalid hexa-decimal*/
	
	char *ptr = getBinary4mHex(hexStr);
	printBinary4mArray(ptr);
	
	printBinary4mHex(hexStr);	
	free(ptr);		
}

void base32Test(char *str32){
	
	char *str32_1 = "abc123";/*valid base32-number*/
    char *str32_2 = "wyz123";/*valid base32-number*/
    
	uint8_t* ptr = getBinary4mBase32(str32);
	printBinary4mArray(ptr);
	
	printBinary4mBase32(str32);	
	free(ptr);
}


void writeBinary_output(uint64_t min,uint64_t max){
	    
   if(writeBinary4mRange(min,max,stdout))
        printf("Successfully written binary\n");
    else
        printf("Not success\n");
}


void writeBinary_file(uint64_t min,uint64_t max){
	
	char *fileName = "binaryRanges.txt";
	FILE * fp = fopen(fileName,"w");
    
   if(writeBinary4mRange(min,max,fp))
        printf("Successfully written all binary to %s file\n",fileName);
    else
        printf("Not success\n");
        
    fclose(fp);
}


//Reading Input.
char *readInputString() {
    char *buf, *tmp_p;
        int i, c;
 
        i = 0;
        buf = (char*)malloc(sizeof(char));
        if (buf == NULL)
                return NULL;
                
 		fseek(stdin,0,SEEK_END);
        c = fgetc(stdin);
 							
	//Read input one by one untill Newline or EOF Occurs.
 							
        while (c != '\n' && c != EOF) {
 							
                buf[i++] = c;
                tmp_p = buf;
                buf = (char*)realloc(buf, (i + 1) * sizeof(char));
                if (buf == NULL) {
                        free(tmp_p);
                        return NULL;
                }
                c = fgetc(stdin);
        }
 
        if (i == 0) {
                free(buf);
                return NULL;
        }
 
        buf[i] = '\0';
        return buf;
}
